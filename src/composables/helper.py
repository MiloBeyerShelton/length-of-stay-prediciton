from typing import List, Literal
import pandas as pd
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error, mean_squared_log_error, r2_score, mean_squared_error

import seaborn as sns


def truncate_column(data: pd.DataFrame, column: str, new_col_name: str, n: int):
    """
    Truncate the column values to n characters
    
    Parameters
    ----------
    data : pd.DataFrame
        The data
    column : str
        The column to truncate
    new_col_name : str
        The new column name to store the truncated values
    n : int
        The number of characters to truncate to
    """
    data[new_col_name] = data[column].apply(lambda x: x[:n])

def split_data(data:pd.DataFrame, split_ration:float):
    """
    Splits the data into training and test data

    Parameters
    ----------
    data : pd.DataFrame
        The data
    split_ration : float
        The ratio to split the data. Here the training to base data ratio is expected
        
    Returns
    -------
    Tuple[pd.DataFrame, pd.DataFrame]
        The training data and test data
    """

    training_data = data.sample(frac=split_ration, random_state=0).copy()
    test_data = data.drop(training_data.index).copy()
    return training_data, test_data

def scale_data(training_data: pd.DataFrame, test_data:pd.DataFrame, columns:List[str]):
    """
    Scales the data using MinMaxScaler

    Parameters
    ----------
    training_data : pd.DataFrame
        The training data
    test_data : pd.DataFrame
        The test data
    columns : List[str]
        The columns to scale

    Returns
    -------
    Tuple[pd.DataFrame, pd.DataFrame]
        The scaled training data and scaled test data
    """
    scaler = MinMaxScaler().fit(training_data[columns])
    scaled_training_data = scaler.transform(training_data[columns])
    scaled_test_data = scaler.transform(test_data[columns])
    return scaled_training_data, scaled_test_data

def find_nearest_core_points(training_data_core_points_scaled:np.ndarray, scaled_test_data:np.ndarray, n_neighbors:int, metric:Literal['manhattan', 'precomputed']):
    """
    finds the nearest core points for each test data point and returns their distances and indices

    Parameters
    ----------
    training_data_core_points_scaled : np.ndarray
        The scaled training data core points
    scaled_test_data : np.ndarray
        The scaled test data
    n_neighbors : int
        The number of nearest neighbors to find
    metric : Literal['manhattan', 'precomputed']
        The metric to use for the nearest neighbors

    Returns
    -------
    Tuple[np.ndarray, np.ndarray]
        The distances and indices of the nearest core points
    """
    nearest_neighbors = NearestNeighbors(metric=metric).fit(training_data_core_points_scaled)
    return nearest_neighbors.kneighbors(X=scaled_test_data, n_neighbors=n_neighbors)

def make_predicitons(nn_distances: np.ndarray, nn_indices:np.ndarray, training_data_core_points:pd.DataFrame):
    """
    Creates a list of predicitons based on the nearest core points for each test data point. the hosp_los is weighted based on the distance to the core points

    Parameters
    ----------
    nn_distances : np.ndarray
        The distances to the nearest core points
    nn_indices : np.ndarray
        The indices of the nearest core points
    training_data_core_points : pd.DataFrame
        The training data core points

    Returns
    -------
    List[float]
        The predicitons for each test data point
    """
    predicitons = []
    for index, i in enumerate(nn_indices):
        #weights = MinMaxScaler().fit_transform(nn_distances[index].reshape(-1,1))
        #weights = 1 - weights

        hosp_los = training_data_core_points.iloc[i]["hosp_los"]
   
        #weighted_mean = np.mean(hosp_los * weights.flatten()) 
        weighted_mean = np.mean(hosp_los ) 
        predicitons.append(weighted_mean)
    return predicitons

def find_test_point_clusters(training_data_core_points_scaled:np.ndarray, training_data_core_points:pd.DataFrame, scaled_test_data:np.ndarray, metric:Literal['manhattan', 'precomputed']):
    """
    
    Parameters
    ----------
    training_data_core_points_scaled : np.ndarray
        The scaled training data core points
    training_data_core_points : pd.DataFrame
        The training data core points
    scaled_test_data : np.ndarray
        The scaled test data
    metric : Literal['manhattan', 'precomputed']
        The metric to use for the nearest neighbors

    Returns
    -------
    List[int]
        The clusters for each test point
    """
    nearest_neighbors = NearestNeighbors(metric=metric).fit(training_data_core_points_scaled)
    distances, indices = nearest_neighbors.kneighbors(X=scaled_test_data, n_neighbors=1)

    test_points_cluster = []
    for i in enumerate(indices):
       test_points_cluster.append(training_data_core_points.iloc[i[1][0]]["cluster"])
    return test_points_cluster




def get_evaluation_metrics(test_data:pd.DataFrame , true_col:str,  prediciton_col:str, error_col:str):
    """
    returns the predictions MAE, MAPE, MSLE, R2, variance and mean

    Parameters
    ----------
    test_data : pd.DataFrame
        The test data
    prediciton_col : str
        The column with the predicitons
    true_col : str
        The column with the true values
    """
    mae = mean_absolute_error(test_data[true_col], test_data[prediciton_col])
    mape = mean_absolute_percentage_error(test_data[true_col], test_data[prediciton_col])
    msle = mean_squared_log_error(test_data[true_col], test_data[prediciton_col])
    r2 = r2_score(test_data[true_col], test_data[prediciton_col])
    mse = mean_squared_error(test_data[true_col], test_data[prediciton_col])
    var = test_data[error_col].var()
    mean = test_data[error_col].mean()
    return mae, mape, msle, r2, mse, var, mean



print_results_template= """
######## Results ########
    MAE:    {mae}
    MAPE:   {mape}
    MSLE:   {msle}
    R2:     {r2}
    MSE:    {mse}
    var:    {var}
    maen:   {mean}
"""

def results_str(test_data:pd.DataFrame, true_col:str, prediciton_col:str, error_col:str):
    """
    returns the evaluation metrics in string form

    Parameters
    ----------
    test_data : pd.DataFrame
        The test data
    true_col : str
        The column with the true values
    prediciton_col : str
        The column with the predicitons
    """
    mae, mape, msle, r2, mse, var, mean = get_evaluation_metrics(test_data, prediciton_col, true_col, error_col)
    #print(f'#### Results #### \n MAE: {mae} \n MAPE: {mape} \n MSLE: {msle} \n R2: {r2} \n Variance: {var} \n Mean: {mean}')
    return print_results_template.format(mae=mae, mape=mape, msle=msle, r2=r2, mse=mse, var=var, mean=mean)

def plot_errors(test_data:pd.DataFrame, error_col:str):
    sns.scatterplot(test_data[error_col])