import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
from kneed import KneeLocator

def get_KDG_eps_range(data: pd.DataFrame, minPts:int, metric=str, eps_cand_sel_step: int= 10):
    """
    get the range of epsilon values from the KDG algorithm

    Parameters
    ----------
    data : pd.DataFrame
        The data
    minPts : int
        The minimum number of points to consider
    metric : str
        The metric to use for the nearest neighbors
    eps_cand_sel_step : int
        The step size to select the epsilon candidates
    """

    nNeighb = NearestNeighbors(n_neighbors=minPts, metric=metric).fit(data)  # creating an object of the NearestNeighbors class
    distances, indices = nNeighb.kneighbors(data)  # finding the nearest neighbours

    distances = np.sort(distances, axis=0)  # sorting the distances
    distances = distances[:, minPts - 1] # unly use the distance to the k-th nearest neighbour
    kneedle = KneeLocator(x=range(1, len(distances) + 1), y=distances, S=1, curve="convex", direction="increasing")

    return distances[:kneedle.knee : eps_cand_sel_step]